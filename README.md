# argus-info-core

## Description

This project defines the Argus info feature
[Clean Code Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
"Enterprise Business Rules" layer (aka "Entities" or "Core"). It provides the core data classes and interfaces for the
TMDb info facility.

This project defines Argus video info interfaces in the innermost Clean Architecture layer.
It exists to specify these interfaces which are used by the video layers to provide Tmdb
acquired data. Tests exist to ensure that the interface designs are sensible. These tests
also show how the interfaces will likely get used.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

For general documentation on Argus, see the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the shelf feature.

## Test Cases

### Overview

The table below identifies the adapter layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions)

| Filename Prefix | Test Case Name                                                               |
|-----------------|------------------------------------------------------------------------------|
| I18nStrings     | When accessing the localized info strings, verify behavior                   |
| InfoKey         | When creating a movie info key instance, verify the result properties        |
|                 | When creating a person info key instance, verify the result properties       |
|                 | When creating a tv info key instance, verify the result properties           |
|                 | When serializing and deserializing a tv show info key, verify by equivalence |
|                 | When serializing and deserializing a movie info key, verify by equivalence   |
|                 | When serializing and deserializing a person info key, verify by equivalence  |
| InfoType        | When using an info key type, verify by name                                  |
| InfoWrapper     | When creating a wrapped movie, verify the properties                         |
|                 | When creating a wrapped person, verify the properties                        |
|                 | When creating a wrapped tv show, verify the properties                       |
|                 | When serializing and deserializing a movie, verify by equivalence            |
|                 | When serializing and deserializing a person, verify by equivalence           |
|                 | When serializing and deserializing a tv show, verify by equivalence          |

### Notes

The single responsibility for this project is to provide the video info interface definitions
used by outer architectural layers. The interfaces adapter layer implements these core video info
interfaces.

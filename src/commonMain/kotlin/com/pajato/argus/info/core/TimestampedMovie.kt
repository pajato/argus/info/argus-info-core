package com.pajato.argus.info.core

import com.pajato.tks.movie.core.Movie
import kotlinx.serialization.Serializable

@Serializable public data class TimestampedMovie(val refreshTime: Timestamp, val movie: Movie) : InfoWrapper()

package com.pajato.argus.info.core

import java.net.URI
import kotlin.collections.MutableMap

public interface InfoRepo {
    public val cache: MutableMap<InfoKey, InfoWrapper>
    public suspend fun injectDependency(uri: URI)
    public suspend fun register(json: String)
    public suspend fun register(item: InfoWrapper)
}

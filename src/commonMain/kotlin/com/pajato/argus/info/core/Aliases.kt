package com.pajato.argus.info.core

public typealias InfoId = Int
public typealias Timestamp = Long

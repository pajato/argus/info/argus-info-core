package com.pajato.argus.info.core

private const val TWENTY_THREE_HOURS = 1000L * 60 * 60 * 23
private const val ONE_SECOND = 1000L

public data class RefreshParams(
    val repo: InfoRepo,
    val refreshCount: Int = Int.MAX_VALUE,
    val refreshPeriod: Long = TWENTY_THREE_HOURS,
    val sendPeriod: Long = ONE_SECOND,
    val onError: (String) -> Unit,
)

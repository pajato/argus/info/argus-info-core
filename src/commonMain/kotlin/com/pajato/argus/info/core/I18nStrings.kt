package com.pajato.argus.info.core

import com.pajato.i18n.strings.StringsResource.put

public object I18nStrings {
    public const val INFO_LOAD_ERROR_KEY: String = "InfoLoadErrorKey"
    public const val INFO_NULL_VALUE_KEY: String = "InfoNullValueKey"
    public const val INFO_URI_ERROR_KEY: String = "InfoUriErrorKey"
    public const val INFO_MISSING_ID: String = "InfoMissingId"
    public const val INFO_NOT_A_FILE: String = "InfoNotAFile"

    public fun registerStrings() {
        put(INFO_LOAD_ERROR_KEY, "Could not load resource at path '{{path}}'!")
        put(INFO_NULL_VALUE_KEY, "The info {{type}} repo has no value for key: {{key}}!")
        put(INFO_URI_ERROR_KEY, "The repo URI is null! A non-null URI value must be injected.")
        put(INFO_MISSING_ID, "A missing refresh data item with key '{{key}}' has been detected!")
        put(INFO_NOT_A_FILE, "The given file with path '{{path}}' is not a valid file!")
    }
}

package com.pajato.argus.info.core

import kotlinx.serialization.Serializable

@Serializable public sealed class InfoWrapper

package com.pajato.argus.info.core

import com.pajato.tks.person.core.Person
import kotlinx.serialization.Serializable

@Serializable public data class TimestampedPerson(val refreshTime: Timestamp, val person: Person) : InfoWrapper()

package com.pajato.argus.info.core

import kotlinx.serialization.Serializable

@Serializable public data class InfoKey(val type: String, val id: InfoId)

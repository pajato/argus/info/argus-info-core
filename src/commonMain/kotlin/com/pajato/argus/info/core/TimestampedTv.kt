package com.pajato.argus.info.core

import com.pajato.tks.tv.core.Tv
import kotlinx.serialization.Serializable

@Serializable public data class TimestampedTv(val refreshTime: Timestamp, val tv: Tv) : InfoWrapper()

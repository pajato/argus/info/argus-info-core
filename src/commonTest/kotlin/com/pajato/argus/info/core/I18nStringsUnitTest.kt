package com.pajato.argus.info.core

import com.pajato.argus.info.core.I18nStrings.INFO_LOAD_ERROR_KEY
import com.pajato.argus.info.core.I18nStrings.INFO_MISSING_ID
import com.pajato.argus.info.core.I18nStrings.INFO_NOT_A_FILE
import com.pajato.argus.info.core.I18nStrings.INFO_NULL_VALUE_KEY
import com.pajato.argus.info.core.I18nStrings.INFO_URI_ERROR_KEY
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.test.ReportingTestProfiler
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class I18nStringsUnitTest : ReportingTestProfiler() {
    @BeforeTest fun setUp() {
        StringsResource.cache.clear()
        I18nStrings.registerStrings()
    }

    @Test fun `When accessing the localized info strings, verify behavior`() {
        assertEquals(LOAD_ERROR_MESSAGE, get(INFO_LOAD_ERROR_KEY, Arg("path", "files/xyzzy")))
        assertEquals(URI_ERROR_MESSAGE, get(INFO_URI_ERROR_KEY))
        assertEquals(NULL_ERROR_MESSAGE, get(INFO_NULL_VALUE_KEY, Arg("type", "TestRepo"), Arg("key", "xyzzy")))
        assertEquals(ID_ERROR_MESSAGE, get(INFO_MISSING_ID, Arg("key", "xyzzy")))
        assertEquals(FILE_ERROR_MESSAGE, get(INFO_NOT_A_FILE, Arg("path", "/xyzzy.txt")))
    }

    companion object {
        const val LOAD_ERROR_MESSAGE = "Could not load resource at path 'files/xyzzy'!"
        const val URI_ERROR_MESSAGE = "The repo URI is null! A non-null URI value must be injected."
        const val NULL_ERROR_MESSAGE = "The info TestRepo repo has no value for key: xyzzy!"
        const val ID_ERROR_MESSAGE = "A missing refresh data item with key 'xyzzy' has been detected!"
        const val FILE_ERROR_MESSAGE = "The given file with path '/xyzzy.txt' is not a valid file!"
    }
}

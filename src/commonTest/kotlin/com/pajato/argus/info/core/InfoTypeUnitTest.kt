package com.pajato.argus.info.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class InfoTypeUnitTest : ReportingTestProfiler() {
    @Test fun `When using an info key type, verify by name`() {
        val keyType = InfoType.Tv
        assertEquals("Tv", keyType.name)
    }
}

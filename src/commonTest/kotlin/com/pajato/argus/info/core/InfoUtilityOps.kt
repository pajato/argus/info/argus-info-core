package com.pajato.argus.info.core

import kotlinx.datetime.Instant
import kotlin.test.Test

/**
 * Convert a date using ISO 8601 or RFC 3339 format to Unix timestamp
 *
 * ISO 8601: This is an international standard that provides a well-defined and unambiguous way to represent dates
 * and times. It's often used in web APIs and data exchange formats.
 *
 * Example: 2024-09-02T14:30:00Z (YYYY-MM-DDTHH:mm:ssZ)
 *
 * RFC 3339: A profile of ISO 8601 commonly used in internet protocols and formats like JSON.
 *
 * Example: 2024-09-02T14:30:00+00:00 (YYYY-MM-DDTHH:mm:ss+ HH: MM)
 */
class InfoUtilityOps {
    @Test fun getStartOfEpochAsDate() { println("${Instant.fromEpochMilliseconds(0L)}") }

    @Test fun convertTimestampToDate() {
        val timestamp = 1726034990885
        println("${Instant.fromEpochMilliseconds(timestamp)}")
    }

    @Test fun convertDateToTimestamp() {
        val isoDate = "2024-07-08T07:20:26.685Z"
        println(Instant.parse(isoDate).toEpochMilliseconds())
    }
}

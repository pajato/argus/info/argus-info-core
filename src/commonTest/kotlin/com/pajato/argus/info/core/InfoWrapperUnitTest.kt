package com.pajato.argus.info.core

import com.pajato.persister.jsonFormat
import com.pajato.tks.movie.core.Movie
import com.pajato.tks.person.core.Person
import com.pajato.tks.tv.core.Tv
import kotlin.test.Test
import kotlin.test.assertEquals

class InfoWrapperUnitTest {

    @Test fun `When creating a wrapped movie, verify the properties`() {
        val refreshTime = 0L
        val movie = Movie()
        val wrapped: InfoWrapper = TimestampedMovie(refreshTime, movie)
        val wrappedMovie = wrapped as TimestampedMovie
        assertEquals(refreshTime, wrappedMovie.refreshTime)
        assertEquals(movie, wrappedMovie.movie)
    }

    @Test fun `When creating a wrapped person, verify the properties`() {
        val refreshTime = 0L
        val person = Person()
        val wrapped: InfoWrapper = TimestampedPerson(refreshTime, person)
        val wrappedPerson = wrapped as TimestampedPerson
        assertEquals(refreshTime, wrappedPerson.refreshTime)
        assertEquals(person, wrappedPerson.person)
    }

    @Test fun `When creating a wrapped tv show, verify the properties`() {
        val refreshTime = 0L
        val tv = Tv()
        val wrapped: InfoWrapper = TimestampedTv(refreshTime, Tv())
        val wrappedTv = wrapped as TimestampedTv
        assertEquals(refreshTime, wrappedTv.refreshTime)
        assertEquals(tv, wrappedTv.tv)
    }

    @Test fun `When serializing and deserializing a movie, verify by equivalence`() {
        val refreshTime = 0L
        val item = Movie()
        val wrapped = TimestampedMovie(refreshTime, item)
        val serialized = jsonFormat.encodeToString(InfoWrapper.serializer(), wrapped)
        val deserialized = jsonFormat.decodeFromString(InfoWrapper.serializer(), serialized)
        assertEquals(wrapped, deserialized)
    }

    @Test fun `When serializing and deserializing a person, verify by equivalence`() {
        val refreshTime = 0L
        val item = Person()
        val wrapped = TimestampedPerson(refreshTime, item)
        val serialized = jsonFormat.encodeToString(InfoWrapper.serializer(), wrapped)
        val deserialized = jsonFormat.decodeFromString(InfoWrapper.serializer(), serialized)
        assertEquals(wrapped, deserialized)
    }

    @Test fun `When serializing and deserializing a tv show, verify by equivalence`() {
        val refreshTime = 0L
        val item = Tv()
        val wrapped = TimestampedTv(refreshTime, item)
        val serialized = jsonFormat.encodeToString(InfoWrapper.serializer(), wrapped)
        val deserialized = jsonFormat.decodeFromString(InfoWrapper.serializer(), serialized)
        assertEquals(wrapped, deserialized)
    }
}

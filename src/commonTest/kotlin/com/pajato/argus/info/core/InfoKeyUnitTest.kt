package com.pajato.argus.info.core

import com.pajato.persister.jsonFormat
import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class InfoKeyUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a movie info key instance, verify the result properties`() {
        val type = InfoType.Movie.name
        val id = 29339
        val infoKey = InfoKey(type, id)
        assertEquals(type, infoKey.type)
        assertEquals(id, infoKey.id)
    }

    @Test fun `When creating a person info key instance, verify the result properties`() {
        val type = InfoType.Person.name
        val id = 190405
        val infoKey = InfoKey(type, id)
        assertEquals(type, infoKey.type)
        assertEquals(id, infoKey.id)
    }

    @Test fun `When creating a tv info key instance, verify the result properties`() {
        val type = InfoType.Tv.name
        val id = 1911
        val infoKey = InfoKey(type, id)
        assertEquals(type, infoKey.type)
        assertEquals(id, infoKey.id)
    }

    @Test fun `When serializing and deserializing a tv show info key, verify by equivalence`() {
        val type = InfoType.Tv.name
        val id = 1911
        val infoKey = InfoKey(type, id)
        val serialized = jsonFormat.encodeToString(InfoKey.serializer(), infoKey)
        val deserialized = jsonFormat.decodeFromString(InfoKey.serializer(), serialized)
        assertEquals(infoKey, deserialized)
    }

    @Test fun `When serializing and deserializing a movie info key, verify by equivalence`() {
        val type = InfoType.Movie.name
        val id = 29339
        val infoKey = InfoKey(type, id)
        val serialized = jsonFormat.encodeToString(InfoKey.serializer(), infoKey)
        val deserialized = jsonFormat.decodeFromString(InfoKey.serializer(), serialized)
        assertEquals(infoKey, deserialized)
    }

    @Test fun `When serializing and deserializing a person info key, verify by equivalence`() {
        val type = InfoType.Person.name
        val id = 0
        val infoKey = InfoKey(type, id)
        val serialized = jsonFormat.encodeToString(InfoKey.serializer(), infoKey)
        val deserialized = jsonFormat.decodeFromString(InfoKey.serializer(), serialized)
        assertEquals(infoKey, deserialized)
    }
}

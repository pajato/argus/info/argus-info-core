package com.pajato.argus.info.core

import java.net.URI

object CustomRefreshInfoRepo : InfoRepo {
    override val cache: MutableMap<InfoKey, InfoWrapper> = mutableMapOf()
    override suspend fun injectDependency(uri: URI) {}
    override suspend fun register(json: String) {}
    override suspend fun register(item: InfoWrapper) {}
}

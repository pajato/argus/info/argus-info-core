package com.pajato.argus.info.core

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

class RefreshParamsUnitTest {

    @Test fun `constructor should initialize all properties correctly with default values`() {
        val refreshParams = RefreshParams(DefaultRefreshInfoRepo) { }
        assertEquals(Int.MAX_VALUE, refreshParams.refreshCount)
        assertEquals(1000L * 60 * 60 * 23, refreshParams.refreshPeriod)
        assertEquals(1000L, refreshParams.sendPeriod)
    }

    @Test fun `constructor should correctly assign custom values`() {
        val onError: (String) -> Unit = {}
        val refreshCount = 5
        val refreshPeriod = 5000L
        val sendPeriod = 2000L
        val refreshParams = RefreshParams(CustomRefreshInfoRepo, refreshCount, refreshPeriod, sendPeriod, onError)
        assertEquals(refreshCount, refreshParams.refreshCount)
        assertEquals(refreshPeriod, refreshParams.refreshPeriod)
        assertEquals(sendPeriod, refreshParams.sendPeriod)
        assertSame(onError, refreshParams.onError)
    }
}
